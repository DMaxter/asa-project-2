#include <iostream>
#include <climits>
#include <algorithm>
#include <queue>
#include <vector>

class Edge{
	private:
		int destination;
		long flow;
		int capacity;
		Edge* twin;
	public:
		Edge(int d, int c);
		int getDestination();
		void setFlow(long f);
		long getFlow();
		int getCapacity();
		void setTwin(Edge *e);
		Edge* getTwin();
};

Edge::Edge(int d, int c){
	this->destination = d;
	this->flow = 0;
	this->capacity = c;
}

int Edge::getDestination(){
	return this->destination;
}

void Edge::setFlow(long f){
	this->flow = f;
}

long Edge::getFlow(){
	return this->flow;
}

int Edge::getCapacity(){
	return this->capacity;
}

void Edge::setTwin(Edge *e){
	this->twin = e;
}

Edge* Edge::getTwin(){
	return this->twin;
}

class SimpleEdge{
	private:
		int u;
		int v;
	public:
		SimpleEdge(int a, int b);
		int getSource();
		int getDestination();
};

SimpleEdge::SimpleEdge(int a, int b){
	this->u = a;
	this->v = b;
}

int SimpleEdge::getSource(){
	return this->u;
}

int SimpleEdge::getDestination(){
	return this->v;
}

class Graph{
	private:
		std::vector<Edge *> *graph;
		int stations;
		int suppliers;
		int total;
	public:
		Graph();
		int getNumVert();
		std::vector<Edge *>* getVertices();
		int getStations();
		int getSuppliers();
};

Graph::Graph(){
	int connections;
	int subtotal;

	// Get values
	std::cin >> this->suppliers >> this->stations >> connections;

	// Vertices plus hiper and source
	this->total = this->suppliers + 2 * this->stations + 2;
	subtotal = this->suppliers + this->stations;

	// Initialization of graph
	this->graph = new std::vector<Edge *>[total];

	// Get capacity of destinations
	int i;
	int c;
	for(i = 1; i <= this->suppliers; i++){
		std::cin >> c;

		// Edges from source to suppliers
		Edge *a = new Edge(total - 1, 0);
		Edge *b = new Edge(i, c);

		a->setTwin(b);
		b->setTwin(a);

		this->graph[i].push_back(a);
		this->graph[total - 1].push_back(b);
	}

	for( ; i <= subtotal; i++){
		std::cin >> c;

		Edge *a = new Edge(this->stations + i, c);
		Edge *b = new Edge(i, 0);

		a->setTwin(b);
		b->setTwin(a);

		this->graph[i].push_back(a);
		this->graph[this->stations + i].push_back(b);
	}

	// Create connections
	int a, b;
	for(i = 0; i < connections; i++){
		std::cin >> a >> b >> c;

		a--;
		b--;

		// Check if needs an auxiliary vertice
		if(a > this->suppliers  && a <= subtotal){
			a += this->stations;
		}

		Edge *u = new Edge(b, c);
		Edge *v = new Edge(a, 0);

		u->setTwin(v);
		v->setTwin(u);

		this->graph[a].push_back(u);
		this->graph[b].push_back(v);
	}
}

int Graph::getNumVert(){
	return this->total;
}

std::vector<Edge *>* Graph::getVertices(){
	return this->graph;
}

int Graph::getStations(){
	return this->stations;
}

int Graph::getSuppliers(){
	return this->suppliers;
}

bool compEdges(SimpleEdge &a, SimpleEdge &b){
	if(a.getSource() == b.getSource()){
		return (a.getDestination() < b.getDestination());
	}

	return (a.getSource() < b.getSource());
}

class PreFlow{
	private:
		std::vector<Edge *> *vertices;
		int total;
		int stations;
		int suppliers;
		int *h;
		int *e;
		bool *cut;
		std::queue<int> L;
		std::vector<SimpleEdge> edges;
		std::vector<int> vert;
		std::vector<Edge *>::iterator *current;

		void initialize();
		void push(int u, Edge* e);
		void relabel(int v);
		void discharge(int v);
		void findCut();
		void cutEdges();
		void printAll();
	public:
		PreFlow(Graph *g){
			this->vertices = g->getVertices();
			this->total = g->getNumVert();
			this->stations =  g->getStations();
			this->suppliers = g->getSuppliers();
			this->h = new int[this->total]();
			this->e = new int[this->total]();
			this->current = new std::vector<Edge *>::iterator[this->total];

			// Define height of source
			this->h[total - 1] = this->total;
		}
		void execute();
		int getMaxFlow();
};

void PreFlow::push(int u, Edge* e){
	int v = e->getDestination();
	int f = e->getFlow();
	int c = e->getCapacity();

	Edge *twin = e->getTwin();

	int df;

	df = std::min(this->e[u], c - f);

	e->setFlow(f + df);
	twin->setFlow(- e->getFlow());


	this->e[u] -= df;
	this->e[v] += df;

	if(v != 0 && v != total - 1){
		this->L.push(v);
	}
}

void PreFlow::relabel(int u){
	int min = INT_MAX;
	std::vector<Edge *>::iterator it;

	for(it = this->vertices[u].begin(); it != this->vertices[u].end(); it++){
		if(((*it)->getCapacity() - (*it)->getFlow() > 0) && (this->h[(*it)->getDestination()] < min)){
			min = this->h[(*it)->getDestination()];
		}
	}

	this->h[u] = min + 1;
}

void PreFlow::initialize(){
	std::vector<Edge *>::iterator it;
	// Get list of suppliers
	int c;
	
	for(it = this->vertices[this->total - 1].begin(); it != this->vertices[this->total - 1].end(); it++){
		c = (*it)->getCapacity();

		(*it)->setFlow(c);
		(*it)->getTwin()->setFlow(-c);

		this->e[(*it)->getDestination()] = c;
		this->e[total - 1] -= c;
	}
}

void PreFlow::discharge(int u){
	std::vector<Edge *>::iterator v;

	while(this->e[u] > 0){
		v = this->current[u];
		if(v == this->vertices[u].end()){
			relabel(u);
			this->current[u] = this->vertices[u].begin();
		}else if(( (long) (*v)->getCapacity() - (*v)->getFlow() > 0) && (this->h[u] == this->h[(*v)->getDestination()] + 1)){
			push(u, *v);
		}else{
			this->current[u]++;
		}
	}

	this->L.pop();
}

void PreFlow::printAll(){
	if(! this->vert.empty()){
		std::vector<int>::iterator it;
		for(it = this->vert.begin(); ;){
			printf("%d", *it);
			it++;

			if(it != this->vert.end()){
				putchar(' ');
			}else{
				break;
			}
		}
	}

	putchar('\n');

	if(! this->edges.empty()){
		std::vector<SimpleEdge>::iterator jt;
		jt = this->edges.begin();
		do{
			printf("%d %d\n", jt->getSource(), jt->getDestination());
			jt++;
		}while(jt != this->edges.end());
	}
}

void PreFlow::findCut(){
	int u;
	std::vector<Edge *>::iterator it;

	this->cut = new bool[total]();
	
	this->L.push(0);

	while(! this->L.empty()){
		u = this->L.front();
		this->L.pop();

		this->cut[u] = true;

		for(it = this->vertices[u].begin(); it != this->vertices[u].end(); it++){
			Edge *twin = (*it)->getTwin();
			if((this->cut[(*it)->getDestination()] == false) && (twin->getCapacity() - twin->getFlow() > 0)){
				this->L.push((*it)->getDestination());
			}
		}
	}
}

void PreFlow::cutEdges(){
	int i;
	std::vector<Edge *>::iterator it;
	int count;

	for(i = total - 2; i > 0; i--){
		// Choose vertices that are not in the cut
		if(this->cut[i] == false){
			count = 0;

			// Run over each of its edges
			for(it = this->vertices[i].begin(); it != this->vertices[i].end(); it++){
				int dest = (*it)->getDestination();

				// Check if edge connects to vertex on the cut and is a full edge
				if((this->cut[dest] == true) && ((*it)->getCapacity() - (*it)->getFlow() == 0)){

					// Check if dest is an auxiliary and change to station
					if(dest > this->stations + this->suppliers){
						dest -= this->stations;
					}

					// Check if both belong to the same station
					if(i == dest){
						this->vert.emplace_back(i + 1);

						int j;
						for(j = 0; j < count; j++){
							this->edges.pop_back();
						}

						break;
					}else{

						// Check if is from auxiliary to another vertex
						if(i > this->stations + this->suppliers){
							this->edges.emplace_back(i - this->stations + 1, dest + 1);
						}else{
							this->edges.emplace_back(i + 1, dest + 1);
						}

						count++;
					}
				}
			}
		}
	}
}

int PreFlow::getMaxFlow(){
	return this->e[0];
}

void PreFlow::execute(){
	int u;

	this->initialize();

	// Initialization of the lists and iterators
	int i;
	for(i = 1; i <= this->suppliers; i++){
		this->L.push(i);
	}

	for(i = this->total - 1; i > 0; i--){
		this->current[i] = this->vertices[i].begin();
	}

	u = this->L.front();
	while(! this->L.empty()){
		this->discharge(u);
		u = this->L.front();
	}

	printf("%d\n", this->getMaxFlow());

	delete[] this->current;
	delete[] this->h;
	delete[] this->e;

	this->findCut();
	this->cutEdges();
	std::sort(this->edges.begin(), this->edges.end(), compEdges);
	std::sort(this->vert.begin(), this->vert.end());
	this->printAll();
}

int main(){
	// Create graph
	Graph graph;

	// Execute Relabel-to Front
	PreFlow algorithm(&graph);

	algorithm.execute();
}
